import express from "express";
import {
  app,
  extRoute,
  initializeApp,
  logger,
  ExpressLibConfig,
  intRoute,
} from "../src/";

const config: ExpressLibConfig = {
  port: 9000,
  log: {
    level: "info",
    format: "json",
  },
  sentryDsn: "",
  region: "",
  env: "",
  commitShortSha: "",
  apigatewayPublicKey: "",
  serviceName: "testing",
};

initializeApp(config);

const dummy = express.Router().get("/", (_, res) => {
  logger.info("sample logging");
  res.status(200).send();
});

// http://localhost:9000/int/service1
intRoute.use("/service1", dummy);

// http://localhost:9000/ext/service2
extRoute.use("/service2", dummy); // throws invalid auth error

// app can be imported anywhere to be extended
app.use("/", dummy);
