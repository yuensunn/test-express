import Joi, { ValidationError } from 'joi';
import express, { Request, Response } from 'express';
import request from 'supertest';
import { createRequestValidator, CustomValidationMessages, getValidationErrorStatusCode } from './validation';
import { captureValidationError } from './expressErrors';

describe('validation', () => {
  it('can validate', async () => {
    const validatorMiddleware = createRequestValidator({
      query: Joi.object({
        number: Joi.number()
          .greater(0)
          .required(),
      }),
    });

    const app = express();
    app.get('/test', [validatorMiddleware], (req: Request, res: Response) => {
      return res.send('hello');
    });
    app.use(captureValidationError);

    const response = await request(app).get('/test?number=-1');
    expect(response.status).toEqual(400);
    expect(response.body).toEqual({
      details: ['"query.number" must be greater than 0'],
      message: '"query.number" must be greater than 0',
    });
  });

  it('can bypass without schema', async () => {
    const missingSchemaValidatorMiddleware = createRequestValidator({});

    const app = express();
    app.get('/test', [missingSchemaValidatorMiddleware], (req: Request, res: Response) => {
      return res.send('hello');
    });
    app.use(captureValidationError);

    const response = await request(app).get('/test?number=-1');
    expect(response.status).toBe(200);
    expect(response.text).toEqual('hello');
  });

  it('extends validatedData instead of replacing', async () => {
    const validatorMiddleware1 = createRequestValidator({
      query: Joi.object({
        number: Joi.number()
          .greater(0)
          .required(),
      }),
    });
    const validatorMiddleware2 = createRequestValidator({
      query: Joi.object({
        anotherNumber: Joi.number()
          .greater(0)
          .required(),
      }),
    });

    const app = express();
    app.get('/test', [validatorMiddleware1, validatorMiddleware2], (req: Request, res: Response) => {
      return res.status(200).json(req.validatedData);
    });
    app.use(captureValidationError);

    const response = await request(app).get('/test?number=1&anotherNumber=2');
    expect(response.status).toEqual(200);
    expect(response.body).toEqual({
      query: {
        number: 1,
        anotherNumber: 2,
      },
    });
  });

  it.each([
    ['', CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE, 'Invalid auth.'],
    ['', CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD, 'Auth payload is invalid.'],
  ])('returns expected custom errors', async (_, key, val) => {
    const validatorMiddleware = createRequestValidator({
      query: Joi.object({
        number: Joi.number().custom((__, helpers) => helpers.error(key)),
      }),
    });

    const app = express();
    app.get('/test', [validatorMiddleware], (req: Request, res: Response) => {
      return res.send('never gonna happen');
    });
    app.use(captureValidationError);

    const response = await request(app).get('/test?number=1');
    expect(response.status).toEqual(500);
    expect(response.body).toEqual({
      message: val,
      details: [val],
    });
  });

  it('can bypass without schema', async () => {
    const missingSchemaValidatorMiddleware = createRequestValidator({});

    const app = express();
    app.get('/test', [missingSchemaValidatorMiddleware], (req: Request, res: Response) => {
      return res.send('hello');
    });
    app.use(captureValidationError);

    const response = await request(app).get('/test?number=-1');
    expect(response.status).toEqual(200);
    expect(response.text).toEqual('hello');
  });
});

describe('getValidationErrorStatusCode', () => {
  it.each([
    CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE,
    CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD,
  ])('returns a 500 if error as a relevant detail type', errorType => {
    const status = getValidationErrorStatusCode(
      new ValidationError('Some error', [{ type: 'A' }, { type: 'B' }, { type: errorType }, { type: 'C' }], null),
    );
    expect(status).toEqual(500);
  });
  it('returns 400 if no relevant type', () => {
    const status = getValidationErrorStatusCode(
      new ValidationError('Some error', [{ type: 'A' }, { type: 'B' }, { type: 'C' }], null),
    );
    expect(status).toEqual(400);
  });
});
