import { logger } from "./logger";
import { app, initialize, ExpressLibConfig } from "./app";
import { extRoute, intRoute } from "./routes";

export {
  logger,
  app,
  initialize as initializeApp,
  intRoute,
  extRoute,
  ExpressLibConfig,
};
