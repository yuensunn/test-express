import * as Sentry from "@sentry/node";
import { ValidationError } from "joi";
import { ApiError } from "./expressErrors";
import { logger } from "./logger";

export type SentryConfig = {
  dsn: string;
  release: string;
  environment: string;
};

export const initializeSentry = ({
  dsn,
  release,
  environment,
}: SentryConfig) => {
  if (!dsn) {
    return;
  }
  logger.info("Initializing Sentry.");
  Sentry.init({
    dsn,
    release,
    environment,
    autoSessionTracking: false,
    beforeSend(event: Sentry.Event, hint?: Sentry.EventHint) {
      const error = hint?.originalException;
      const is4xx =
        error instanceof ValidationError ||
        (error instanceof ApiError && error.status < 500);
      if (is4xx) {
        return null;
      }
      return event;
    },
  });
};
