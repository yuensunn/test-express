import { ValidationError } from 'joi';
import jwt from 'jsonwebtoken';
import { validateAuth } from './middleware';
import { CustomValidationMessages } from './validation';

describe('validateAuth', () => {
  const verifySpy = jest.spyOn(jwt, 'verify').mockReturnValue({
    userId: 'success',
  } as any);
  let request: any;
  const response: any = {};
  const next = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    request = { headers: { authorization: 'whatever' } };
  });

  it('fails out if missing authorization', async () => {
    request = { headers: {} };
    validateAuth(request, response, next);
    expect(request.validatedData).toBeUndefined();
    expect(next).toHaveBeenCalledWith(expect.any(ValidationError));
    expect(next.mock.calls[0][0].details[0].type).toEqual(CustomValidationMessages.AUTH_TOKEN_EMPTY);
  });

  it('fails out if verify throws', async () => {
    verifySpy.mockImplementationOnce(() => {
      throw new Error('whatever');
    });
    validateAuth(request, response, next);
    expect(request.validatedData).toBeUndefined();
    expect(next).toHaveBeenCalledWith(expect.any(ValidationError));
    expect(next.mock.calls[0][0].details[0].type).toEqual(CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE);
  });

  it('fails out if verify returns as a string', async () => {
    verifySpy.mockReturnValueOnce('a string' as any);
    validateAuth(request, response, next);
    expect(request.validatedData).toBeUndefined();
    expect(next).toHaveBeenCalledWith(expect.any(ValidationError));
    expect(next.mock.calls[0][0].details[0].type).toEqual(CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD);
  });

  it('fails out if verify returns no userId', async () => {
    verifySpy.mockReturnValueOnce({ something: 'else' } as any);
    validateAuth(request, response, next);
    expect(request.validatedData).toBeUndefined();
    expect(next).toHaveBeenCalledWith(expect.any(ValidationError));
    expect(next.mock.calls[0][0].details[0].type).toEqual(CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD);
  });

  it('succeeds', async () => {
    verifySpy.mockReturnValueOnce({ userId: 'whatever' } as any);
    validateAuth(request, response, next);
    expect(request.validatedData).toEqual({
      headers: { authorization: { userId: 'whatever' } },
    });
    expect(next).toHaveBeenCalled();
  });
});
