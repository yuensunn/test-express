import { NextFunction, Request, Response } from 'express';
import Joi, { ValidationError } from 'joi';
import merge from 'lodash.merge';

interface Schema {
  headers?: Joi.Schema;
  body?: Joi.Schema;
  params?: Joi.Schema;
  query?: Joi.Schema;
}

export enum CustomValidationMessages {
  AUTH_TOKEN_VALIDATION_FAILURE = 'auth.token_validation_failure',
  AUTH_TOKEN_MISSING_PAYLOAD = 'auth.token_missing_payload',
  AUTH_TOKEN_EMPTY = 'auth.token_empty',
}
const systemFailureMessageTypes = new Set<string>([
  CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE,
  CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD,
  CustomValidationMessages.AUTH_TOKEN_EMPTY,
]);
21;
export const getValidationErrorStatusCode = (err: ValidationError): number => {
  return err.details.find(item => systemFailureMessageTypes.has(item.type)) ? 500 : 400;
};

export const createRequestValidator = (schema: Schema) => (req: Request, res: Response, next: NextFunction): void => {
  if (!schema) {
    next();
    return;
  }
  const { headers, body, params, query } = req;
  const data = { headers, body, params, query };
  const { error, value } = Joi.object(schema)
    .messages({
      [CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE]: 'Invalid auth.',
      [CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD]: 'Auth payload is invalid.',
      [CustomValidationMessages.AUTH_TOKEN_EMPTY]: 'Auth is empty',
    })
    .validate(data, {
      abortEarly: true,
      allowUnknown: true,
      stripUnknown: true,
    });
  if (error) {
    next(error);
    return;
  }
  req.validatedData = merge(req.validatedData, value);
  next();
};
