/* eslint-disable @typescript-eslint/no-unused-vars */
// stackoverflow https://stackoverflow.com/a/68641378
import express from 'express';

import { JwtPayload } from 'jsonwebtoken';

type Payload = JwtPayload & {
  userId: string;
};
declare global {
  namespace Express {
    interface Request {
      validatedData?: {
        body?: any;
        params?: any;
        query?: any;
        headers?: {
          authorization?: Payload;
          [key: string]: any;
        };
      };
    }
  }
}
