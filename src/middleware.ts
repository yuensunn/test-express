import Joi from "joi";
import jwt, { JwtPayload } from "jsonwebtoken";
import { createRequestValidator, CustomValidationMessages } from "./validation";
import { logger } from "./logger";

let API_GATEWAY_PUBLIC_KEY = "";

const validateAPIGatewayJWT = (
  value: string,
  helpers: Joi.CustomHelpers
): JwtPayload | Joi.ErrorReport => {
  try {
    const token = value.replace("Bearer ", "");
    const payload = jwt.verify(token, API_GATEWAY_PUBLIC_KEY);
    if (typeof payload === "string" || !payload?.userId) {
      logger.error(`Invalid payload: ${payload}`);
      return helpers.error(CustomValidationMessages.AUTH_TOKEN_MISSING_PAYLOAD);
    }
    return payload;
  } catch (e) {
    logger.error(e);
    return helpers.error(
      CustomValidationMessages.AUTH_TOKEN_VALIDATION_FAILURE
    );
  }
};

export const validateAuthSchema = {
  headers: Joi.object({
    authorization: Joi.string()
      .required()
      .custom(validateAPIGatewayJWT)
      .error((errors) => {
        const error = errors[0];
        if (error.code === "any.required" || error.code === "string.empty") {
          error.code = CustomValidationMessages.AUTH_TOKEN_EMPTY;
        }
        return error;
      }),
  }).required(),
};

export const validateAuth = createRequestValidator(validateAuthSchema);

export const initializeMiddleware = (apiGatewatPublicKey: string) => {
  API_GATEWAY_PUBLIC_KEY = apiGatewatPublicKey;
};
