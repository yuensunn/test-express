import jwt from "jsonwebtoken";

export default () => {
  if (!process.env.JWT_PRIVATE_KEY) {
    console.log("process.env.JWT_PRIVATE_KEY not set up");
    return;
  }

  return jwt.sign(
    { _id: "user-id", userId: "user-account-id" },
    process.env.JWT_PRIVATE_KEY!.replace(/\\n/g, "\n"),
    {
      algorithm: "RS256",
      expiresIn: "365d",
    }
  );
};
