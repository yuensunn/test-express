import { Command } from "commander";
import generateJwt from "./generate-local-jwt";

const program = new Command();

program
  .command("gen-jwt")
  .description("Generate local jwt token")
  .action((str, options) => {
    console.log(generateJwt());
  });

program.parse();
