import express from "express";
import { validateAuth } from "./middleware";

const intRoute = express.Router();
const extRoute = express.Router();
extRoute.use(validateAuth);

export { intRoute, extRoute };
