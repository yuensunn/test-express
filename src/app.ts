import * as Sentry from "@sentry/node";
import express, { Express } from "express";
import bodyParser from "body-parser";
import { logger, initializeLogger } from "./logger";
import { initializeSentry } from "./sentry";
import { LoggerOptions } from "winston";
import { extRoute, intRoute } from "./routes";
import { initializeMiddleware } from "./middleware";

export let app: Express;

type logFormat = "json" | "prettyPrint";

export type ExpressLibConfig = {
  port: number;
  log: {
    level: LoggerOptions["level"];
    format: logFormat;
  };
  sentryDsn: string;
  region: string;
  env: string;
  commitShortSha: string;
  serviceName: string;
  apigatewayPublicKey: string;
};

export const initialize = (config: ExpressLibConfig) => {
  app = express();

  initializeLogger({
    level: config.log.level,
    logFormat: config.log.format,
    name: config.serviceName,
  });
  initializeSentry({
    dsn: config.sentryDsn,
    environment: `${config.region}-${config.env}`,
    release: `${config.serviceName}@${config.commitShortSha}`,
  });
  initializeMiddleware(config.apigatewayPublicKey);

  app.use(Sentry.Handlers.requestHandler());
  app.use(bodyParser.json());

  app.use("/ext", extRoute);
  app.use("/int", intRoute);

  app.get("*/health", (req, res) => {
    res.setHeader("Cache-Control", "no-cache");
    res.send({ status: "OK" });
  });

  app.listen(config.port, () => {
    logger.info(`Listening on port ${config.port}`);
  });
};

