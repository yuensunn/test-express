import { Express, NextFunction, Request, Response } from "express";
import { ValidationError } from "joi";
import Sentry from "@sentry/node";
import * as validation from "./validation";
import { logger } from "./logger";

export abstract class ApiError extends Error {
  abstract message: string;

  abstract status: number;

  // Calling this when json-ing below is a default JSON.stringify behaviour
  toJSON = () => ({
    message: this.message,
  });
}

// Intentional errors that we throw.
export const captureApiError = (
  err: Error | ApiError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!(err instanceof ApiError)) {
    next(err);
    return;
  }
  const isClientError = err.status < 500;

  const logType = isClientError ? "warn" : "error";
  const stack = isClientError ? undefined : err.stack;

  const { message, status } = err;
  logger[logType](message, { stack, status });
  res.status(err.status).json({
    message: err.message,
  });
};

export type ErrorResponse = {
  message: string;
  details?: any;
};

// Errors thrown from the Joi validation framework
export const captureValidationError = (
  err: Error | ValidationError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!(err instanceof ValidationError)) {
    next(err);
    return;
  }

  const errorDetails = err.details.map((d) => d.message);

  const payload: ErrorResponse = {
    message: err.message,
    details: errorDetails,
  };

  const status = validation.getValidationErrorStatusCode(err);

  const logLevel = status >= 500 ? "error" : "warn";
  logger[logLevel](errorDetails.join(" | "), { status });
  res.status(status).json(payload);
};

// All other errors.
export const captureUnhandledError = (
  err: Error,
  req: Request,
  res: Response,
  _: NextFunction
) => {
  const errorMessage = err.toString();
  logger.error(errorMessage, { stack: err.stack });
  const errorPayload: ErrorResponse = {
    message: err.message,
    details: errorMessage,
  };
  res.status(500).json(errorPayload);
};

// Error handlers are registered with the app and should be done last.
// See https://expressjs.com/en/guide/error-handling.html
export const initializeErrorHandling = (app: Express) => {
  app.use(Sentry.Handlers.errorHandler());
  app.use(captureValidationError);
  app.use(captureApiError);
  app.use(captureUnhandledError);
};
