import { format, transports, createLogger, LoggerOptions } from "winston";

export type LoggerConfig = {
  level: LoggerOptions["level"];
  name: string;
  logFormat: "json" | "prettyPrint";
};
const _createLogger = ({ level, name, logFormat }: LoggerConfig) =>
  createLogger({
    level: level,
    defaultMeta: { service: name },
    format: format.combine(
      format.timestamp({
        format: "YYYY-MM-DD HH:mm:ss",
      }),
      format.errors({ stack: true }),
      format.splat(),
      format[logFormat]()
    ),
    transports: new transports.Console({
      handleExceptions: true,
      handleRejections: true,
    }),
  });

export let logger: ReturnType<typeof _createLogger>;

export const initializeLogger = (config: LoggerConfig) => {
  logger = _createLogger(config);
  return logger;
};
