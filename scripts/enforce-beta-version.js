const { version } = require('../package.json');

if (!version.includes('beta')) {
  console.error('ERROR: Package version should include "beta".');
  process.exit(1);
}
